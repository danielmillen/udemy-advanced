import React, { useState, useRef, useMemo } from 'react';
import { Animated, StyleSheet, PanResponder, Dimensions } from 'react-native';
import Card from '../components/Card';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SWIPE_THRESHOLD = 0.25 * SCREEN_WIDTH;
const SWIPE_OUT_DURATION = 250;

const Deck = ({
  data,
  onSwipeRight = () => {
    console.log('right');
  },
  onSwipeLeft = () => {
    console.log('left');
  },
}) => {
  const [index, setIndex] = useState(0);
  const position = useRef(new Animated.ValueXY()).current;

  const resetPosition = () => {
    Animated.spring(position, { toValue: { x: 0, y: 0 } }).start();
  };

  const onSwipeComplete = (direction) => {
    direction === 'right'
      ? onSwipeRight(data[index])
      : onSwipeLeft(data[index]);
    position.setValue({ x: 0, y: 0 });
    console.log(index.toString());
    setIndex(index + 1);
  };

  const forceSwipe = (direction, idx) => {
    console.log(idx);
    const x = (direction === 'right' ? SCREEN_WIDTH : -SCREEN_WIDTH) * 1.25;
    Animated.timing(position, {
      toValue: { x, y: 0 },
      duration: SWIPE_OUT_DURATION,
    }).start(() => onSwipeComplete(direction));
  };

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gesture) => {
        position.setValue({ x: gesture.dx, y: gesture.dy });
      },
      onPanResponderRelease: (event, gesture) => {
        if (SWIPE_THRESHOLD < gesture.dx) {
          forceSwipe('right', index);
        } else if (-SWIPE_THRESHOLD > gesture.dx) {
          forceSwipe('left');
        } else {
          resetPosition();
        }
      },
    })
  ).current;

  const getCardStyle = () => {
    const rotate = position.x.interpolate({
      inputRange: [-SCREEN_WIDTH * 2.0, 0, SCREEN_WIDTH * 2.0],
      outputRange: ['-120deg', '0deg', '120deg'],
    });

    return {
      ...position.getLayout(),
      transform: [
        {
          rotate,
        },
      ],
    };
  };

  const Cards = data.map((c, idx) => {
    if (idx < index) {
      return null;
    }

    if (idx === index) {
      return (
        <Animated.View
          style={getCardStyle()}
          {...panResponder.panHandlers}
          key={c.id}
        >
          <Card title={c.text} image={c.uri} {...panResponder.panHandlers} />
        </Animated.View>
      );
    }

    return <Card title={c.text} image={c.uri} key={c.id} />;
  });

  console.log('Global:', index);
  return <>{Cards}</>;
};

const styles = StyleSheet.create({});

export default Deck;
