import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Button, Card as CardElement } from 'react-native-elements';

const Card = ({ title, image }) => {
  return (
    <CardElement title={title} image={{ uri: image }}>
      <Button
        icon={{ name: 'code', color: '#FFF' }}
        buttonStyle={{ backgroundColor: '#03A9F4' }}
        title="Click Me!"
      />
    </CardElement>
  );
};

const styles = StyleSheet.create({});

export default Card;
