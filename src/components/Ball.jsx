import React, { useEffect, useState, useRef } from 'react';
import { View, StyleSheet, Animated } from 'react-native';

const Ball = () => {
  const springAnim = useRef(new Animated.ValueXY(0, 0)).current;

  const move = () => {
    Animated.spring(springAnim, { toValue: { x: 200, y: 500 } }).start();
  };

  useEffect(() => {
    move();
  }, []);

  return (
    <Animated.View style={springAnim.getLayout()}>
      <View style={styles.ball} />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  ball: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderWidth: 30,
    borderColor: 'black',
  },
});

export default Ball;
